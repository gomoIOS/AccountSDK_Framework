//
//  GMAccountCenterApiManager.h
//  GLive
//
//  Created by Gordon Su on 17/4/11.
//  Copyright © 2017年 tencent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <GOMONetSDK/GMNetHTTPResponse.h>


#define kVerificationCodeTypeRegister   @"REGISTER" //注册
#define kVerificationCodeTypeLogin      @"LOGIN" //登陆
#define kVerificationCodeTypeBinding    @"BINDING" //手机绑定
#define kVerificationCodeTypeUnbinding  @"UNBINDING" //手机绑定
#define kVerificationCodeTypeRetrievePwd @"RETRIVE_PWD" //找回密码

#define kRegisterTypeVisitor             @"visitor"
#define kRegisterTypeMobile             @"mobile"
#define kRegisterTypeFacebook           @"facebook"
#define kRegisterTypeGoogle             @"google"
#define kRegisterTypeTwitter            @"twitter"
#define kRegisterTypeInstagram          @"instagram"

#define kOpenConnectTypeFacebook        @"facebook"
#define kOpenConnectTypeGoogle          @"google"
#define kOpenConnectTypeTwitter         @"twitter"

#define kBindingTypeMobileToOpenID      @"MOBILE_TO_OPEN_ID"
#define kBindingTypeOpebIDToMobile      @"OPEN_ID_TO_MOBILE"


@class GMAccountTokenInfo;

typedef void (^GMAccountCenterApiCompleteBlock) (GMNetHTTPResponse *response);

@interface GMAccountCenterApiManager : NSObject

+ (GMAccountCenterApiManager *)sharedManager;

#pragma mark - 第三方登陆接口

/**
 第三方登陆接口 POST HTTP BASIC 认证
 //资源路径：/v2/{open_connect_type}/login
 @param openConnectType 第三方登陆类型
 @param openId 第三方登陆Id 非空
 @param vistorId 非必需项，如果客户端需要将已存在的游客用户转化为第三方登陆用户，则传此值
 @param open_connect_user_name 非空
 @param complete <#complete description#>
 */
- (void)loginThirdPartyWithOpenConnectType:(NSString *)openConnectType openId:(NSString *)openId visitorId:(NSString *)vistorId openConnectUserName:(NSString *)open_connect_user_name complete:(GMAccountCenterApiCompleteBlock)complete;

#pragma mark - 游客接口

/**
 游客注册登录 POST Access Token认证
 //资源路径：/v2/vistor/register
 @param policy 取值有IGNORE,服务直接忽略此注册请求，返回已存在的用户信息OVERRIDE,服务器覆盖已存在的注册用户的相关信（注册时间，用户数据信息版本号等REPORT服务器返回错误，告诉客户端该用户已存在
 @param complete <#complete description#>
 */
- (void)loginWithVistorRegisterWithRepeatedDidPolicy:(NSString *)policy complete:(GMAccountCenterApiCompleteBlock)complete DEPRECATED_MSG_ATTRIBUTE("暂时不支持游客登录");
#pragma mark - Token接口
/**
 刷新token post
 //资源路径：/v2/token/refresh

 @param complete <#complete description#>
 */
- (void)refreshTokencomplete:(GMAccountCenterApiCompleteBlock)complete;
#pragma mark - 注册用户信息通用操作接口
/**
 退出登陆 POST HTTP BASIC 认证
 退出后，之前所分配的access token将被置为无效
 @param registerType <#registerType description#>
 @param complete <#complete description#>
 */
- (void)logoutWithRegisterType:(NSString *)registerType complete:(GMAccountCenterApiCompleteBlock)complete;

/**
 获取用户的账号基本信息 GET Access Token认证
资源路径：/v2/{register_type}/basic
 @param register_typ 注册方式的小写形式，例如：mobile	非空
 @param complete <#complete description#>
 */
- (void)fetchUserBasicInfo:(NSString *)register_type complete:(GMAccountCenterApiCompleteBlock)complete;

/**
 获取用户的profile信息 GET Access Token认证
 资源路径：/v2/{register_type}/profile
 @param register_type 注册方式的小写形式，例如：mobile
 @param complete <#complete description#>
 */
- (void)fetchUserProfileWithRegisterType:(NSString *)register_type complete:(GMAccountCenterApiCompleteBlock)complete ;

/**
 新增或更新用户的profile信息 PUT
 资源路径：/v2/{register_type}/profile
 @param profileJsonData 只需设置需要更新的字段信息即可
 @param complete <#complete description#>
 */
- (void)updataUserProfile:(NSDictionary *)profileJsonDic registerType:(NSString *)register_type complete:(GMAccountCenterApiCompleteBlock)complete ;

//资源路径：/v2/{register_type}/attach_info

/**
 获取用户的所有附加信息 GET Access Token认证
 资源路径：资源路径：/v2/{register_type}/attach_info
 @param register_type 注册方式的小写形式，例如：mobile 非空
 @param complete <#complete description#>
 */
- (void)fetchUserAttachInfoWithRegisterType:(NSString *)register_type complete:(GMAccountCenterApiCompleteBlock)complete;

//资源路径：/v2/{register_type}/attach_info/segment

/**
 获取用户的某一部分附加信息 POST Access Token认证
资源路径：/v2/{register_type}/attach_info/segment
 @param register_type 注册方式的小写形式，例如：mobile
 @param complete <#complete description#>
 */
- (void)fetchUserAttachInfoSegmentWithRegisterType:(NSString *)register_type complete:(GMAccountCenterApiCompleteBlock)complete;

//资源路径：/v2/{register_type}/attach_info

/**
 新增或更新用户的附加信息 PUT Access Token认证
 资源路径：资源路径：/v2/{register_type}/attach_info
 @param register_type 注册方式的小写形式，例如：mobile 非空
 @param JsonObject 更新的键值对信息，其键为用JsonPath表示的key，其值为对应的更新值
 @param complete <#complete description#>
 */
- (void)updataUserAttachInfoWithRegisterType:(NSString *)register_type jsonObject:(id)JsonObject complete:(GMAccountCenterApiCompleteBlock)complete;

#pragma mark - 手机接口

/**
 手机号码密码登录 POST HTTP BASIC 认证
 资源路径：/v2/mobile/{phonenumber}/login/password

 @param phoneNumber <#phoneNumber description#>
 @param password <#password description#>
 @param complete <#complete description#>
 */
- (void)loginWithPhone:(NSString *)phoneNumber password:(NSString *)password complete:(GMAccountCenterApiCompleteBlock)complete;
    
/**
 验证码登录
 
 @param phoneNumber <#phoneNumber description#>
 @param auto_register 如果该手机号之前尚未注册，是否自动注册
 @param complete <#complete description#>
 */
- (void)loginWithMobileSMSVerificationCode:(NSString *)phoneNumber auto_register:(BOOL)auto_register complete:(GMAccountCenterApiCompleteBlock)complete;

/**
 发送短信验证码 POST HTTP BASIC 认证
 * 资源路径：/v2/mobile/{phonenumber}/verification_code

 @param phone <#phone description#>
 @param complete <#complete description#>
 */
- (void)getMobileSMSVerificationCode:(NSString *)phone verificationCodeType:(NSString *)verificationCodeType complete:(GMAccountCenterApiCompleteBlock)complete;

/**
 验证短信验证码 POST HTTP BASIC 认证
 资源路径：/v2/mobile/{phonenumber}/verification_code/{verificationCode}
 @param phone <#phone description#>
 @param code <#code description#>
 @param verificationCodeType 验证的短信验证码类型,REGISTER 注册 LOGIN 登陆 BINDING 手机绑定 RETRIVE_PWD 找回密码
 @param complete <#complete description#>
 */
- (void)checkMobileSMSVerificationCode:(NSString *)phone code:(NSString *)code verificationCodeType:(NSString *)verificationCodeType complete:(GMAccountCenterApiCompleteBlock)complete;

/**
 * 注册 POST HTTP BASIC 认证
 * 调用此接口需要注册短信验证码权限
 * 用户注册成功后，默认用户密码为空字符串，是否需要为用户设置密码由客户端决定
 * 如果客户端需要为注册用户设置密码，则调用重置密码接口，旧密码传空字符串
 * 资源路径：/v2/mobile/{phonenumber}/register

 @param phone 格式为国家号码格式或标准的E.164格式,如果号码格式为国家号码格式，国家信息取至Device中的country字段
 @param password 使用SHA256对原始密码进行哈希,非必需项，如果指定，表示注册时同时设定密码
 @param vistor_id 非必需项，如果客户端需要将已存在的游客用户转化为手机注册用户，则传此值
 @param complete <#complete description#>
 */
- (void)registerWithPhone:(NSString *)phone password:(NSString *)password visitorId:(NSString *)visitorId complete:(GMAccountCenterApiCompleteBlock)complete;

/**
 重置密码 PUT HTTP BASIC 认证
 资源路径：/v2/mobile/{phonenumber}/password/reset
 @param phone 路径参数
 @param oldPassword 旧密码（大写形式）,使用SHA256对原始密码进行哈希
 @param newPassword 新密码（大写形式）使用SHA256对原始密码进行哈希
 @param complete <#complete description#>
 */
- (void)resetPassWordWithPhone:(NSString *)phone oldPassword:(NSString *)oldPassword newPassword:(NSString *)newPassword complete:(GMAccountCenterApiCompleteBlock)complete;

/**
 找回密码,调用此接口需要找回密码短信验证码权限 PUT HTTP BASIC 认证
/v2/mobile/{phonenumber}/password/retrive
 @param phone <#phone description#>
 @param password <#password description#>
 @param complete <#complete description#>
 */
- (void)retrivePasswordWithPhone:(NSString *)phone password:(NSString *)password complete:(GMAccountCenterApiCompleteBlock)complete;

/**
 手机绑定 POST Access Token认证
/v2/mobile/binding
 @param phone <#phone description#>
 @param bindingType 取值有
 MOBILE_TO_OPEN_ID 已存在的手机号绑定未存在的第三方登陆账号
 OPEN_ID_TO_MOBILE 已存在的第三方登陆账号绑定绑定未存在的手机号
 @param openConnectType <#openConnectType description#>
 @param openId <#openId description#>
 @param complete <#complete description#>
 */
- (void)mobileBindingWithPhone:(NSString *)phone bindingType:(NSString *)bindingType openConnectType:(NSString *)openConnectType openId:(NSString *)openId complete:(GMAccountCenterApiCompleteBlock)complete;

/**
 调用此接口需要解绑手机短信验证码权限（UNBINDING ）
/v2/mobile/unbinding POST Access Token认证
 @param phone <#phone description#>
 @param bindingType 取值有
 MOBILE_TO_OPEN_ID 使用手机号解绑第三方登陆账号
 OPEN_ID_TO_MOBILE 使用第三方登陆账号解绑手机号
 @param openConnectType <#openConnectType description#>
 @param openId <#openId description#>
 @param complete <#complete description#>
 */
- (void)mobileUnbindingWithPhone:(NSString *)phone bindingType:(NSString *)bindingType openConnectType:(NSString *)openConnectType openId:(NSString *)openId complete:(GMAccountCenterApiCompleteBlock)complete;

/**
 获取绑定账号列表 GET Access Token认证
/v2/{register_type}/binding
 @param register_type <#register_type description#>
 @param complete <#complete description#>
 */
- (void)fetchBindingWithRegisterType:(NSString *)register_type complete:(GMAccountCenterApiCompleteBlock)complete;

/**
 绑定:绑定操作为使用已存在的注册用户绑定不存在的注册用户
 注册用户之间支持互相绑定，绑定和解绑手机需要验证码授权，如果注册方式不支持绑定操作，将返回错误码NOT_SUPPORTED
/v2/{register_type}/binding POST Access Token认证
 @param register_type <#register_type description#>
 @param binding_register_type <#binding_register_type description#>
 @param binding_identity_id <#binding_identity_id description#>
 @param binding_account_attach_info <#binding_account_attach_info description#>
 @param complete <#complete description#>
 */
- (void)bindingWithRegisterType:(NSString *)register_type bindingRegisterType:(NSString *)binding_register_type bindingIdentityId:(NSString *)binding_identity_id bindingAccountAttachInfo:(NSDictionary *)binding_account_attach_info complete:(GMAccountCenterApiCompleteBlock)complete;

/**
 解绑:解绑手机需要验证码授权，如果注册方式不支持绑定操作，将返回错误码NOT_SUPPORTED
/v2/{register_type}/unbinding POST Access Token认证
 @param register_type <#register_type description#>
 @param unbinding_register_type <#unbinding_register_type description#>
 @param unbinding_identity_id <#unbinding_identity_id description#>
 @param complete <#complete description#>
 */
- (void)unbindingWithRegisterType:(NSString *)register_type unbindingRegisterType:(NSString *)unbinding_register_type unbindingIdentityId:(NSString *)unbinding_identity_id complete:(GMAccountCenterApiCompleteBlock)complete;





//更换绑定的手机号
- (void)changePhoneWithOldPhonenumber:(NSString *)oldPhonenumber newPhonenumber:(NSString *)newPhonenumber complete:(GMAccountCenterApiCompleteBlock)complete;

//test
//产品版本管理服务
//http://version.api.goforandroid.com/api/v3/product/versions?app_key=${app_key}&device=${device}&timestamp=${timestamp}
- (void)checkAppVersionComplete:(GMAccountCenterApiCompleteBlock)complete;

//http://version.api.goforandroid.com/api/v1/product/versions?product_id=1016&version_number=20&channel=200&country=CN&lang=zh
- (void)checkAppVersionWithProductId:(NSString *)product_id versionNumber:(NSString *)version_number channel:(NSString *)channel country:(NSString *)country lang:(NSString *)lan complete:(GMAccountCenterApiCompleteBlock)complete;

@end
