//
//  GLHttpTokenInfo.h
//  GLive
//
//  Created by Gordon Su on 17/4/11.
//  Copyright © 2017年 tencent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreGraphics/CoreGraphics.h>

@interface GMAccountTokenInfo : NSObject

@property (nonatomic, copy) NSString *access_token;

/**
 以秒表示的access_token的有效期
 */
@property (nonatomic) CGFloat expired_in;

@property (nonatomic, copy) NSString *refresh_token;

@property (nonatomic,copy) NSDate *lastRecordDate;

@end
